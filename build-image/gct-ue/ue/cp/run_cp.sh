#!/bin/sh

#sleep 2

echo "Starting CP part.."
/usr/local/bin/qemu-system-i386 -m 256 -cpu host --enable-kvm \
	-device isa-debug-exit,iobase=0xf4,iosize=0x04 -no-reboot -nographic \
	-net none -pidfile qemu.pid \
	-chardev pty,id=con,mux=on -serial chardev:con -mon chardev=con,mode=readline -rtc clock=vm \
	-device ivshmem-doorbell,vectors=1,chardev=ivshmem -chardev socket,path=/tmp/ivshmem_socket,id=ivshmem \
	-device ueshmem-doorbell,vectors=1,chardev=ueshmem -chardev socket,path=/tmp/uev,id=ueshmem \
	-kernel ./zp-nr.elf

