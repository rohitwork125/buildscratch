#!/bin/bash
#If not already running then start it!
pid=$(pidof localPubSub)
if [[ "$pid" == "" ]]; then
  cd "${0%/*}"
  ../build/localPubSub #> /dev/null 2>&1 &
  #echo "Running on pid: '$(pidof localPubSub)'"
else
   echo "Already running on pid: '$pid'"
fi

